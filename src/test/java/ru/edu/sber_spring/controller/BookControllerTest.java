package ru.edu.sber_spring.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.edu.sber_spring.dto.AuthorDTO;
import ru.edu.sber_spring.dto.BookAuthorDTO;
import ru.edu.sber_spring.jwtsecurity.JwtTokenUtil;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.service.userdetails.CustomUserDetailsService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class BookControllerTest {

	@Autowired
	public MockMvc mvc;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	private String token;

	private static final String ROLE_USER_NAME = "test";

	private HttpHeaders generateHeaders() {
		HttpHeaders headers = new HttpHeaders();
		token = generateToken(ROLE_USER_NAME);
		headers.add("Authorization", "Bearer " + token);
		return headers;
	}

	private String generateToken(String userName) {
		return jwtTokenUtil.generateToken(customUserDetailsService.loadUserByUsername(userName));
	}

	@Test
	public void list() throws Exception {
		mvc.perform(
						get("/rest/books/list")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void create() throws Exception {
		AuthorDTO authorDTO = new AuthorDTO();
		authorDTO.setAuthorFIO("TESTEROV TEST TESTEROVICH");
		authorDTO.setLifePeriod("2022-2023");
		authorDTO.setDescription("Author for TEST PURPOSE");
		authorDTO.setId(6L);
		Book book = Book.builder()
				.title("test book")
				.build();
		BookAuthorDTO bookAuthorDTO = new BookAuthorDTO(book, List.of(authorDTO));

		mvc.perform(
						post("/rest/books/add")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.content(asJsonString(bookAuthorDTO))
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void update() throws Exception {
		AuthorDTO authorDTO = new AuthorDTO();
		authorDTO.setAuthorFIO("TESTEROV TEST TESTEROVICH");
		authorDTO.setLifePeriod("2022-2023");
		authorDTO.setDescription("Author for TEST PURPOSE");
		authorDTO.setId(6L);
		Book book = Book.builder()
				.title("test book")
				.build();
		BookAuthorDTO bookAuthorDTO = new BookAuthorDTO(book, List.of(authorDTO));

		mvc.perform(
						put("/rest/books/update")
								.param("bookId", "5")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.content(asJsonString(bookAuthorDTO))
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void deleteBook() throws Exception {
		mvc.perform(
						delete("/rest/books/delete")
								.param("bookId", "5")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void getOne() throws Exception {
		mvc.perform(
						get("/rest/books/getBook")
								.param("bookId", "7")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void addAuthor() throws Exception {
		mvc.perform(
						get("/rest/books/getBook")
								.param("bookId", "7")
								.param("authorId", "10")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void search() throws Exception {
		mvc.perform(
						get("/rest/books/search")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	public String asJsonString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
