package ru.edu.sber_spring.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.edu.sber_spring.dto.PublishDTO;
import ru.edu.sber_spring.jwtsecurity.JwtTokenUtil;
import ru.edu.sber_spring.service.userdetails.CustomUserDetailsService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class PublishingControllerTest {

	@Autowired
	public MockMvc mvc;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	private String token;

	private static final String ROLE_USER_NAME = "test";

	private HttpHeaders generateHeaders() {
		HttpHeaders headers = new HttpHeaders();
		token = generateToken(ROLE_USER_NAME);
		headers.add("Authorization", "Bearer " + token);
		return headers;
	}

	private String generateToken(String userName) {
		return jwtTokenUtil.generateToken(customUserDetailsService.loadUserByUsername(userName));
	}

	@Test
	public void list() throws Exception {
		mvc.perform(
						get("/rest/publishing/list")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void create() throws Exception {
		PublishDTO publishDTO = new PublishDTO();
		publishDTO.setUserId(4L);
		publishDTO.setBookId(7L);
		publishDTO.setReturnDate(LocalDateTime.now());
		publishDTO.setReturnDateOutput(LocalDateTime.now());
		publishDTO.setRentPeriod(1);
		publishDTO.setReturned(false);
		publishDTO.setAmount(1);

		mvc.perform(
						post("/rest/publishing/add")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.content(asJsonString(publishDTO))
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void update() throws Exception {
		PublishDTO publishDTO = new PublishDTO();
		publishDTO.setUserId(4L);
		publishDTO.setBookId(7L);
		publishDTO.setReturnDate(LocalDateTime.now());
		publishDTO.setReturnDateOutput(LocalDateTime.now());
		publishDTO.setRentPeriod(1);
		publishDTO.setReturned(false);
		publishDTO.setAmount(1);

		mvc.perform(
						put("/rest/publishing/update")
								.param("publishingId", "6")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.content(asJsonString(publishDTO))
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void deletePublish() throws Exception {
		mvc.perform(
						delete("/rest/publishing/delete")
								.param("publishingId", "6")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void getOne() throws Exception {
		mvc.perform(
						get("/rest/publishing/getPublishing")
								.param("publishingId", "12")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void close() throws Exception {
		mvc.perform(
						post("/rest/publishing/close")
								.param("publishingId", "12")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	public String asJsonString(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		try {
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
