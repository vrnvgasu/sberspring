package ru.edu.sber_spring.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.edu.sber_spring.dto.UserDTO;
import ru.edu.sber_spring.jwtsecurity.JwtTokenUtil;
import ru.edu.sber_spring.service.userdetails.CustomUserDetailsService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class UserControllerTest {

	@Autowired
	public MockMvc mvc;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	private String token;

	private static final String ROLE_USER_NAME = "test";

	private HttpHeaders generateHeaders() {
		HttpHeaders headers = new HttpHeaders();
		token = generateToken(ROLE_USER_NAME);
		headers.add("Authorization", "Bearer " + token);
		return headers;
	}

	private String generateToken(String userName) {
		return jwtTokenUtil.generateToken(customUserDetailsService.loadUserByUsername(userName));
	}

	@Test
	public void list() throws Exception {
		mvc.perform(
						get("/rest/users/list")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void create() throws Exception {
		UserDTO userDTO = new UserDTO();
		userDTO.setRoleId(1L);
		userDTO.setLogin("login");
		userDTO.setFirstName("user");
		userDTO.setLastName("user");
		userDTO.setPassword("qwerty");
		userDTO.setBirthDate("2022-01-01");
		userDTO.setPhone("555");
		userDTO.setAddress("Moscow");
		userDTO.setBackUpEmail("1@1.com");

		mvc.perform(
						post("/rest/users/add")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.content(asJsonString(userDTO))
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void update() throws Exception {
		UserDTO userDTO = new UserDTO();
		userDTO.setRoleId(1L);
		userDTO.setLogin("login");
		userDTO.setFirstName("user");
		userDTO.setLastName("user");
		userDTO.setPassword("qwerty");
		userDTO.setBirthDate("2022-01-01");
		userDTO.setPhone("555");
		userDTO.setAddress("Moscow");
		userDTO.setBackUpEmail("1@1.com");

		mvc.perform(
						put("/rest/users/update")
								.param("userId", "6")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.content(asJsonString(userDTO))
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void deleteUser() throws Exception {
		mvc.perform(
						delete("/rest/users/delete")
								.param("userId", "6")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void getOne() throws Exception {
		mvc.perform(
						get("/rest/users/getUser")
								.param("id", "4")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	@Test
	public void getLateBooks() throws Exception {
		mvc.perform(
						get("/rest/users/4/getLateBooks")
								.headers(generateHeaders())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
				)
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}

	public String asJsonString(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		try {
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
