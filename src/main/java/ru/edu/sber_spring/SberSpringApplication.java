package ru.edu.sber_spring;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.Transactional;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.model.Publishing;
import ru.edu.sber_spring.model.Role;
import ru.edu.sber_spring.model.RoleEnum;
import ru.edu.sber_spring.model.User;
import ru.edu.sber_spring.repository.AuthorRepository;
import ru.edu.sber_spring.repository.BookRepository;
import ru.edu.sber_spring.repository.PublishingRepository;
import ru.edu.sber_spring.repository.RoleRepository;
import ru.edu.sber_spring.repository.UserRepository;

@SpringBootApplication
@EnableScheduling
public class SberSpringApplication implements CommandLineRunner {

	private final AuthorRepository authorRepository;

	private final BookRepository bookRepository;

	private final PublishingRepository publichingRepository;

	private final RoleRepository roleRepository;

	private final UserRepository userRepository;

	@Autowired
	public SberSpringApplication(AuthorRepository authorRepository, BookRepository bookRepository,
			PublishingRepository publichingRepository, RoleRepository roleRepository, UserRepository userRepository) {
		this.authorRepository = authorRepository;
		this.bookRepository = bookRepository;
		this.publichingRepository = publichingRepository;
		this.roleRepository = roleRepository;
		this.userRepository = userRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(SberSpringApplication.class, args);
	}

	@Override
	public void run(String... args) {
//		initData();
	}

	@Transactional
	public void initData() {
		Role roleAdmin = roleRepository.findByTitle(RoleEnum.ADMIN);
		if (Objects.isNull(roleAdmin)) {
			roleAdmin = Role.builder().
					title(RoleEnum.ADMIN)
					.build();
			roleRepository.save(roleAdmin);
		}
		Role roleUser = roleRepository.findByTitle(RoleEnum.USER);
		if (Objects.isNull(roleUser)) {
			roleUser = Role.builder().
					title(RoleEnum.USER)
					.build();
			roleRepository.save(roleUser);
		}

		User user1 = userRepository.findByLogin("user1");
		if (Objects.isNull(user1)) {
			user1 = User.builder().
					firstName("1")
					.lastName("User")
					.login("user1")
					.backUpEmail("user1@gmail.com")
					.password("qwerty")
					.build();
			user1.setRole(roleUser);
			userRepository.save(user1);
		}
		User user2 = userRepository.findByLogin("user2");
		if (Objects.isNull(user2)) {
			user2 = User.builder().
					firstName("2")
					.lastName("User")
					.login("user2")
					.backUpEmail("user2@gmail.com")
					.password("qwerty")
					.build();
			user2.setRole(roleUser);
			userRepository.save(user2);
		}

		Author author1 = authorRepository.findByAuthorFIO("fio1");
		if (Objects.isNull(author1)) {
			author1 = Author.builder()
					.authorFIO("fio1")
					.build();
			authorRepository.save(author1);
		}
		Author author2 = authorRepository.findByAuthorFIO("fio2");
		if (Objects.isNull(author2)) {
			author2 = Author.builder()
					.authorFIO("fio2")
					.build();
			authorRepository.save(author2);
		}

		Book book1 = bookRepository.findByTitle("book1");
		if (Objects.isNull(book1)) {
			book1 = Book.builder()
					.title("book1")
					.build();
			bookRepository.save(book1);
			book1.setAuthors(Set.of(author1, author2));
			bookRepository.save(book1);
		}
		Book book2 = bookRepository.findByTitle("book2");
		if (Objects.isNull(book2)) {
			book2 = Book.builder()
					.title("book2")
					.build();
			bookRepository.save(book2);
			book2.setAuthors(Set.of(author2));
			bookRepository.save(book2);
		}

		Publishing publishing1 = publichingRepository.findByUserId(user1.getId());
		if (Objects.isNull(publishing1)) {
			publishing1 = Publishing.builder()
					.rentDate(LocalDateTime.now())
					.returnDate(LocalDateTime.now().plusMonths(1))
					.returnPeriod(1)
					.returned(false)
					.build();
			publichingRepository.save(publishing1);
			publishing1.setUser(user1);
			publishing1.setBook(book1);
			publichingRepository.save(publishing1);
		}
		Publishing publishing2 = publichingRepository.findByUserId(user2.getId());
		if (Objects.isNull(publishing2)) {
			publishing2 = Publishing.builder()
					.rentDate(LocalDateTime.now())
					.returnDate(LocalDateTime.now().plusMonths(1))
					.returnPeriod(1)
					.returned(false)
					.build();
			publichingRepository.save(publishing2);
			publishing2.setUser(user2);
			publishing2.setBook(book2);
			publichingRepository.save(publishing2);
		}

		//////////////////
//		bookRepository.deleteById(book1.getId());
//		userRepository.deleteById(user1.getId());
//		publichingRepository.deleteById(publishing1.getId());
//		authorRepository.deleteById(author1.getId());
	}

}
