package ru.edu.sber_spring.MVC.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.sber_spring.dto.UserDTO;
import ru.edu.sber_spring.service.UserService;

@Controller
@Slf4j
@RequestMapping("/users")
public class MVCUserController {

	private final UserService userService;

	public MVCUserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/")
	public String index(HttpServletRequest httpServletRequest, Model model) {
		return "index";
	}

	@GetMapping("/registration")
	public String registrationPage(@ModelAttribute("userForm") UserDTO userDTO) {
		return "registration";
	}

	@PostMapping("/registration")
	public String createUser(@Valid @ModelAttribute("userForm") UserDTO userDTO,
			BindingResult result) {
		if (result.hasErrors()) {
			return "registration";
		} else {
			log.info("CОЗДАНИЕ НОВОГО ПОЛЬЗОВАТЕЛЯ: " + userDTO.toString());
			userService.createFromDTO(userDTO);
			return "redirect:/login";
		}
	}

	@GetMapping("/remember-password")
	public String changePassword() {
		return "rememberPassword";
	}

	@PostMapping("/remember-password")
	public String changePassword(@ModelAttribute("changePasswordForm") @Valid UserDTO userDTO) {
		log.info("!!!Changing password!!!!");
		userDTO = userService.getUserByEmail(userDTO.getBackUpEmail());
		userService.sendChangePasswordEmail(userDTO.getBackUpEmail(), userDTO.getId());
		return "redirect:/login";
	}

	@GetMapping("/change-password/{userId}")
	public String changePasswordAfterEmailSent(@PathVariable Long userId, Model model) {
		model.addAttribute("userId", userId);
		return "changePassword";
	}

	@PostMapping("/change-password/{userId}")
	public String changePassword(@PathVariable Long userId,
			@ModelAttribute("changePasswordForm") @Valid UserDTO userDTO) {
		userService.changePassword(userId, userDTO.getPassword());
		return "redirect:/login";
	}

	@GetMapping("/profile/{userId}")
	public String profile(@PathVariable Long userId,
			Model model) {
		model.addAttribute("user", userService.getOne(userId));
		return "/users/viewProfile";
	}

	@GetMapping("/profile/update/{userId}")
	public String update(@PathVariable Long userId,
			Model model) {
		model.addAttribute("user", userService.getOne(userId));
		return "/users/updateProfile";
	}

	@PostMapping("/profile/update")
	public String update(@ModelAttribute("userForm") UserDTO userDTO,
			HttpServletRequest request) {
		Long userId = Long.valueOf(request.getParameter("id"));
		userService.updateFromDTO(userDTO, userId);
		return "redirect:/users/profile/" + userId;
	}

}
