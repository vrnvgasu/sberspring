package ru.edu.sber_spring.MVC.controller;

import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.sber_spring.dto.AuthorDTO;
import ru.edu.sber_spring.service.AuthorService;
import ru.edu.sber_spring.service.BookAuthorService;

@Controller
@RequestMapping("/authors")
public class MVCAuthorController {

	private final AuthorService authorService;

	private final BookAuthorService bookAuthorService;

	public MVCAuthorController(AuthorService authorService, BookAuthorService bookAuthorService) {
		this.authorService = authorService;
		this.bookAuthorService = bookAuthorService;
	}

	@GetMapping("")
	public String getAll(Model model) {
		model.addAttribute("authors", authorService.listAll());
		return "authors/viewAllAuthors";
	}

	@GetMapping("/{authorId}")
	public String getAuthorWithBooks(@PathVariable Long authorId, Model model) {
		model.addAttribute("authorWithBooks", bookAuthorService.getAuthorWithBooks(authorId));
		return "authors/viewAuthor";
	}

	@GetMapping("/add")
	public String create() {
		return "/authors/addAuthor";
	}

	@PostMapping("/add")
	public String create(@ModelAttribute("authorForm") @Valid AuthorDTO authorDTO) {
		authorService.createFromDTO(authorDTO);
		return "redirect:/authors";
	}

	@GetMapping("/add-book/{authorId}")
	public String createBookToAuthor(@PathVariable Long authorId, Model model) {
		//показать форму для добавления книги существующей к автору
		model.addAttribute("books", bookAuthorService.getAllBooksWhereAuthorNotIn(authorId));
		model.addAttribute("author", authorService.getOne(authorId));
		return "/authors/addAuthorBook";
	}

	@PostMapping("/add-book/{authorId}")
	public String createBookToAuthor(@PathVariable Long authorId, @RequestParam("bookId") Long bookId) {
		bookAuthorService.addBookToAuthor(bookId, authorId);
		return "redirect:/authors/" + authorId;
	}

	@PostMapping("/search")
	public String searchByAuthorFIO(@ModelAttribute("authorForm") @Valid AuthorDTO authorDTO,
			Model model) {
		model.addAttribute("authors", authorService.findAuthorByAuthorFIO(authorDTO.getAuthorFIO()));
		return "authors/viewAllAuthors";
	}

}
