package ru.edu.sber_spring.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.sber_spring.dto.AuthorDTO;
import ru.edu.sber_spring.dto.BookAuthorDTO;
import ru.edu.sber_spring.dto.BookDTO;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.service.AuthorService;
import ru.edu.sber_spring.service.GenericService;

@RestController
@RequestMapping("/rest/authors")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Авторы",
		description = "Контроллер для работы с авторами нашей библиотеки.")
public class AuthorController
		extends GenericController<Author> {

	private final GenericService<Author, AuthorDTO> authorService;

	private final GenericService<Book, BookAuthorDTO> bookService;

	public AuthorController(GenericService<Author, AuthorDTO> authorService,
			GenericService<Book, BookAuthorDTO> bookService) {
		this.authorService = authorService;
		this.bookService = bookService;
	}

	@Operation(description = "Получить информацию об одном авторе по его ID",
			method = "getOne")
	@RequestMapping(value = "/getAuthor", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Author> getOne(@RequestParam(value = "id") Long id) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(authorService.getOne(id));
	}

	@Operation(description = "Получить информацию о всех авторах",
			method = "listAll")
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Author>> listAllAuthors() {
		return ResponseEntity.status(HttpStatus.OK)
				.body(authorService.listAll());
	}

	@Operation(description = "Создать автора",
			method = "add")
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Author> add(@RequestBody AuthorDTO newAuthor) {
		return ResponseEntity.status(HttpStatus.CREATED).body(authorService.createFromDTO(newAuthor));
	}

	@Operation(description = "Обновить автора",
			method = "update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Author> updateAuthor(@RequestBody AuthorDTO author,
			@RequestParam(value = "authorId") Long authorId) {
		return ResponseEntity.status(HttpStatus.CREATED).body(authorService.updateFromDTO(author, authorId));
	}

	@Operation(description = "Удалить автора",
			method = "delete")
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestParam(value = "authorId") Long authorId) {
		authorService.delete(authorId);
		return ResponseEntity.status(HttpStatus.OK).body("Автор успешно удален");
	}

	@Operation(description = "Добавить автору книгу",
			method = "addBook")
	@RequestMapping(value = "/addBook", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Author> addBook(@RequestParam(value = "bookId") Long bookId,
			@RequestParam(value = "authorId") Long authorId) {
		Author author = authorService.getOne(authorId);
		Book book = bookService.getOne(bookId);

		book.getAuthors().add(author);
		author.getBooks().add(book);

		return ResponseEntity.status(HttpStatus.CREATED).body(authorService.update(author));
	}

	@Operation(description = "Получить информацию обо всех книгах автора")
	@RequestMapping(value = "/{authorId}/getBooks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<BookDTO>> getAuthorBooks(@PathVariable(value = "authorId") Long authorId) {
		return ResponseEntity.status(HttpStatus.OK).body(((AuthorService) authorService).getAllAuthorBooks(authorId));
	}

}
