package ru.edu.sber_spring.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.sber_spring.dto.RoleDTO;
import ru.edu.sber_spring.model.Role;
import ru.edu.sber_spring.service.GenericService;

@RestController
@RequestMapping("/rest/roles")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Роли",
		description = "Контроллер для работы с ролями нашей библиотеки.")
public class RoleController
		extends GenericController<Role> {

	private final GenericService<Role, RoleDTO> roleService;

	public RoleController(GenericService<Role, RoleDTO> roleService) {
		this.roleService = roleService;
	}

	@Operation(description = "Получить информацию о роли по ее ID",
			method = "getOne")
	@RequestMapping(value = "/getRole", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Role> getOne(@RequestParam(value = "id") Long id) {
		return ResponseEntity.status(HttpStatus.OK).body(roleService.getOne(id));
	}

	@Operation(description = "Получить информацию о всех ролях",
			method = "listAll")
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Role>> listAll() {
		return ResponseEntity.status(HttpStatus.OK).body(roleService.listAll());
	}

	@Operation(description = "Создать роль",
			method = "add")
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Role> add(@RequestBody RoleDTO roleDTO) {
		roleDTO.setCreatedWhen(LocalDateTime.now());
		return ResponseEntity.status(HttpStatus.CREATED).body(roleService.createFromDTO(roleDTO));
	}

	@Operation(description = "Обновить роль",
			method = "update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Role> update(@RequestBody RoleDTO roleDTO,
			@RequestParam(value = "roleId") Long roleId) {
		return ResponseEntity.status(HttpStatus.CREATED).body(roleService.updateFromDTO(roleDTO, roleId));
	}

	@Operation(description = "Удалить роль",
			method = "delete")
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestParam(value = "roleId") Long roleId) {
		roleService.delete(roleId);
		return ResponseEntity.status(HttpStatus.OK).body("Роль успешно удалена");
	}

}
