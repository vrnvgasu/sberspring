package ru.edu.sber_spring.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.sber_spring.dto.PublishDTO;
import ru.edu.sber_spring.model.Publishing;
import ru.edu.sber_spring.service.GenericService;
import ru.edu.sber_spring.service.PublishService;

@RestController
@RequestMapping("/rest/publishing")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Выдачи",
		description = "Контроллер для работы с выдачей книг нашей библиотеки.")
public class PublishingController
		extends GenericController<Publishing> {

	private final GenericService<Publishing, PublishDTO> publichingService;

	public PublishingController(GenericService<Publishing, PublishDTO> publichingService) {
		this.publichingService = publichingService;
	}

	@Operation(description = "Получить информацию о выдаче книги по ее ID",
			method = "getOne")
	@RequestMapping(value = "/getPublishing", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Publishing> getOne(@RequestParam(value = "publishingId") Long publishingId) {
		return ResponseEntity.status(HttpStatus.OK).body(publichingService.getOne(publishingId));
	}

	@Operation(description = "Получить информацию о всех выдачах",
			method = "listAll")
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Publishing>> listAll() {
		return ResponseEntity.status(HttpStatus.OK).body(publichingService.listAll());
	}

	@Operation(description = "Создать выдачу",
			method = "add")
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Publishing> add(@RequestBody PublishDTO publishingDTO) {
		publishingDTO.setCreatedWhen(LocalDateTime.now());

		return ResponseEntity.status(HttpStatus.CREATED).body(publichingService.createFromDTO(publishingDTO));
	}

	@Operation(description = "Обновить выдачу",
			method = "update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Publishing> update(@RequestBody PublishDTO publishingDTO,
			@RequestParam(value = "publishingId") Long publishingId) {
		return ResponseEntity.status(HttpStatus.CREATED).body(publichingService.updateFromDTO(publishingDTO, publishingId));
	}

	@Operation(description = "Удалить выдачу",
			method = "delete")
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestParam(value = "publishingId") Long publishingId) {
		publichingService.delete(publishingId);
		return ResponseEntity.status(HttpStatus.OK).body("Выдача успешно удалена");
	}

	@Operation(description = "Закрыть выдачу",
			method = "close")
	@RequestMapping(value = "/close", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Publishing> close(@RequestParam(value = "publishingId") Long publishingId) {
		return ResponseEntity.status(HttpStatus.CREATED).body(((PublishService) publichingService).close(publishingId));
	}

}
