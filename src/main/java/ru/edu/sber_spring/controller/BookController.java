package ru.edu.sber_spring.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.sber_spring.dto.AuthorDTO;
import ru.edu.sber_spring.dto.BookAuthorDTO;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.model.GenreEnum;
import ru.edu.sber_spring.service.BookService;
import ru.edu.sber_spring.service.GenericService;

@RestController
@RequestMapping("/rest/books")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Книги",
		description = "Контроллер для работы с книгами нашей библиотеки.")
public class BookController
		extends GenericController<Book> {

	private final GenericService<Book, BookAuthorDTO> bookService;

	private final GenericService<Author, AuthorDTO> authorService;

	public BookController(GenericService<Book, BookAuthorDTO> bookService,
			GenericService<Author, AuthorDTO> authorService) {
		this.bookService = bookService;
		this.authorService = authorService;
	}

	@Operation(description = "Получить информацию об одной книге по ее ID",
			method = "getOne")
	@RequestMapping(value = "/getBook", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> getOne(@RequestParam(value = "bookId") Long bookId) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(bookService.getOne(bookId));
	}

	@Operation(description = "Получить информацию о всех книгах",
			method = "listAll")
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Book>> listAllBooks() {
		return ResponseEntity.status(HttpStatus.OK).body(bookService.listAll());
	}

	@Operation(description = "Создать книгу",
			method = "add")
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> add(@RequestBody BookAuthorDTO newBook) {
		return ResponseEntity.status(HttpStatus.CREATED).body(bookService.createFromDTO(newBook));
	}

	@Operation(description = "Обновить книгу",
			method = "update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> updateBook(@RequestBody BookAuthorDTO book,
			@RequestParam(value = "bookId") Long bookId) {
		return ResponseEntity.status(HttpStatus.CREATED).body(bookService.updateFromDTO(book, bookId));
	}

	@Operation(description = "Удалить книгу",
			method = "delete")
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestParam(value = "bookId") Long bookId) {
		bookService.delete(bookId);
		return ResponseEntity.status(HttpStatus.OK).body("Книга успешно удалена");
	}

	@Operation(description = "Добавить книге автора",
			method = "addAuthor")
	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Book> addAuthor(@RequestParam(value = "bookId") Long bookId,
			@RequestParam(value = "authorId") Long authorId) {
		Author author = authorService.getOne(authorId);
		Book book = bookService.getOne(bookId);

		book.getAuthors().add(author);

		return ResponseEntity.status(HttpStatus.CREATED).body(bookService.update(book));
	}

	@Operation(description = "Поиск книги по названию/автору/жанру", method = "getUsersLateBooks")
	@RequestMapping(value = "/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Book>> search(@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "authorFio", required = false) String authorFio,
			@RequestParam(value = "genre", required = false) GenreEnum genre) {
		return ResponseEntity.status(HttpStatus.OK).body(((BookService) bookService).search(title, authorFio, genre));
	}

}
