package ru.edu.sber_spring.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.sber_spring.dto.LatePublishingBookDTO;
import ru.edu.sber_spring.dto.UserDTO;
import ru.edu.sber_spring.model.User;
import ru.edu.sber_spring.service.GenericService;
import ru.edu.sber_spring.service.UserService;

@RestController
@RequestMapping("/rest/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Пользователи",
		description = "Контроллер для работы с пользователями нашей библиотеки.")
public class UserController
		extends GenericController<User> {

	private final GenericService<User, UserDTO> userService;

	public UserController(GenericService<User, UserDTO> userService) {
		this.userService = userService;
	}

	@Operation(description = "Получить информацию о пользователи по его ID",
			method = "getOne")
	@RequestMapping(value = "/getUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<User> getOne(@RequestParam(value = "id") Long id) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(userService.getOne(id));
	}

	@Operation(description = "Получить информацию о всех пользователях",
			method = "listAll")
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> listAll() {
		return ResponseEntity.status(HttpStatus.OK).body(userService.listAll());
	}

	@Operation(description = "Создать пользователя",
			method = "add")
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> add(@RequestBody UserDTO userDTO) {
		userDTO.setCreatedWhen(LocalDateTime.now());
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.createFromDTO(userDTO));
	}

	@Operation(description = "Обновить пользователя",
			method = "update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> update(@RequestBody UserDTO userDTO,
			@RequestParam(value = "userId") Long userId) {
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.updateFromDTO(userDTO, userId));
	}

	@Operation(description = "Удалить пользователя",
			method = "delete")
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestParam(value = "userId") Long userId) {
		userService.delete(userId);
		return ResponseEntity.status(HttpStatus.OK).body("Пользователь успешно удален");
	}

	@Operation(description = "Получить информацию обо всех несданных вовремя книг пользователя")
	@RequestMapping(value = "/{userId}/getLateBooks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<LatePublishingBookDTO>> getUsersLateBooks(@PathVariable(value = "userId") Long userId) {
		return ResponseEntity.status(HttpStatus.OK).body(((UserService) userService).getAllUsersLateReturnedBooks(userId));
	}

}
