package ru.edu.sber_spring.dto;

import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import ru.edu.sber_spring.model.Publishing;
import ru.edu.sber_spring.model.User;

@Getter
@Setter
public class UserPublishDTO
		extends UserDTO {

	private Set<PublishDTO> publishDTOSet;

	public UserPublishDTO(User user) {
		super(user);
		Set<Publishing> publishes = user.getPublishingSet();
		Set<PublishDTO> publishDTOS = new HashSet<>();
		for (Publishing publish : publishes) {
			PublishDTO publishDTO = new PublishDTO(publish);
			publishDTOS.add(publishDTO);
		}
		this.publishDTOSet = publishDTOS;
	}

}
