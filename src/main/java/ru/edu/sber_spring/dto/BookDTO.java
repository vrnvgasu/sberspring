package ru.edu.sber_spring.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.model.GenreEnum;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO
		extends CommonDTO {

	private Long id;

	private String title;

	private GenreEnum genre;

	private String onlineCopy;

	private String storagePlace;

	private Integer amount;

	private String publishYear;

	public BookDTO(final Book book) {
		this.id = book.getId();
		this.amount = book.getAmount();
		this.genre = book.getGenre();
		this.onlineCopy = book.getOnlineCopy();
		this.publishYear = book.getPublishYear();
		this.storagePlace = book.getStoragePlace();
		this.title = book.getTitle();
	}

}
