package ru.edu.sber_spring.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.sber_spring.model.Publishing;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class LatePublishingBookDTO
		extends PublishDTO {

	private Integer latePeriod;

	private BookDTO bookDTO;

	public LatePublishingBookDTO(final Publishing publishing) {
		super(publishing);
	}

}
