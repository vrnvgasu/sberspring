package ru.edu.sber_spring.dto;

import lombok.Data;
import ru.edu.sber_spring.model.GenreEnum;

@Data
public class BookSearchDTO {

	private String bookTitle;

	private String authorFio;

	private GenreEnum genre;

}
