package ru.edu.sber_spring.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.model.Publishing;
import ru.edu.sber_spring.model.User;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class PublishDTO
		extends CommonDTO {

	private Long id;

	private Long userId;

	private Long bookId;

	private Boolean returned;

	private LocalDateTime returnDate;

	private Integer rentPeriod;

	private LocalDateTime returnDateOutput;

	private User user;
	private Book book;
	private Integer amount;

	private String bookTitleSearch;

	private boolean delayed;

	public PublishDTO(User user,
			Book book,
			Integer amount,
			Integer period) {
		this.user = user;
		this.book = book;
		this.amount = amount;
		this.returnDateOutput = LocalDateTime.now();
		this.returned = false;
		this.rentPeriod = period;
		this.returnDate = returnDateOutput.plusMonths(Long.valueOf(rentPeriod));
	}

	public PublishDTO(Publishing publish) {
		this.id = publish.getId();
		this.user = publish.getUser();
		this.book = publish.getBook();
		this.returnDate = publish.getRentDate();
		this.returned = publish.getReturned();
		this.rentPeriod = publish.getReturnPeriod();
		this.returnDateOutput = publish.getReturnDate();
		this.amount = publish.getAmount();
		this.delayed = publish.getReturnDate().isBefore(LocalDate.now().atStartOfDay()) && !publish.getReturned();
	}

}
