package ru.edu.sber_spring.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.sber_spring.model.Author;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AuthorDTO
		extends CommonDTO {

	private Long id;

	private String authorFIO;

	private String lifePeriod;

	private String description;

	public AuthorDTO(final Author author) {
		this.id = author.getId();
		this.authorFIO = author.getAuthorFIO();
		this.lifePeriod = author.getLifePeriod();
		this.description = author.getDescription();
	}

	public AuthorDTO(String authorFIO) {
		this.authorFIO = authorFIO;
	}

}
