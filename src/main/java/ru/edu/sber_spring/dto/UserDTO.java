package ru.edu.sber_spring.dto;

import java.time.format.DateTimeFormatter;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.edu.sber_spring.model.User;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserDTO
		extends CommonDTO {

	private Long id;

	private RoleDTO role;

	private Long roleId;

	@NotEmpty(message = "Поле не должно быть пустым")
	private String login;

	@NotEmpty(message = "Поле не должно быть пустым")
	private String firstName;

	@NotEmpty(message = "Поле не должно быть пустым")
	private String lastName;

	private String middleName;

	@NotEmpty(message = "Поле не должно быть пустым")
	@Size(min = 3, message = "Пароль должен быть больше трех символов")
//    @Pattern()
	private String password;

	@NotEmpty(message = "Поле не должно быть пустым")
	@Email
	private String backUpEmail;

	@NotEmpty(message = "Поле не должно быть пустым")
	private String birthDate;

	@NotEmpty(message = "Поле не должно быть пустым")
	private String phone;

	@NotEmpty(message = "Поле не должно быть пустым")
	private String address;

	public UserDTO(final User user) {
		this.role = new RoleDTO(user.getRole());
		this.id = user.getId();
		this.login = user.getLogin();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.middleName = user.getMiddleName();
		this.password = user.getPassword();
		this.backUpEmail = user.getBackUpEmail();
		this.phone = user.getPhone();
		this.birthDate = user.getBirthDate().format(DateTimeFormatter.ISO_DATE);
		this.address = user.getAddress();
	}

}
