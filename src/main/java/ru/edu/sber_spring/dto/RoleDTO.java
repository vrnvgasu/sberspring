package ru.edu.sber_spring.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.sber_spring.model.Role;
import ru.edu.sber_spring.model.RoleEnum;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class RoleDTO
		extends CommonDTO {

	private Long id;

	private RoleEnum title;

	private String description;

	public RoleDTO(Role role) {
		this.id = role.getId();
		this.title = role.getTitle();
		this.description = role.getDescription();
	}

}
