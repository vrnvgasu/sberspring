package ru.edu.sber_spring.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.sber_spring.model.Author;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AuthorBookDTO
		extends AuthorDTO {

	private List<BookDTO> bookDTOS;

	public AuthorBookDTO(final Author author, List<BookDTO> bookDTOS) {
		super(author);
		this.bookDTOS = bookDTOS;
	}

}
