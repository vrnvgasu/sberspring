package ru.edu.sber_spring.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.sber_spring.model.Book;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookAuthorDTO
		extends BookDTO {

	private Set<Long> authorIds = new HashSet<>();

	private List<AuthorDTO> authorDTOs;

	public BookAuthorDTO(Book book,
			List<AuthorDTO> authorDTOs) {
		super(book);
		this.authorDTOs = authorDTOs;

		for (AuthorDTO authorDTO: this.authorDTOs) {
			authorIds.add(authorDTO.getId());
		}
	}

}
