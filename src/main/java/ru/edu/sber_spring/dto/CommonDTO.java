package ru.edu.sber_spring.dto;

import java.time.LocalDateTime;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CommonDTO {

	private String createdBy;

	private LocalDateTime createdWhen;

}
