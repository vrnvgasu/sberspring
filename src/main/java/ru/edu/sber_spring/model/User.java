package ru.edu.sber_spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.LocalDate;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "users_seq", allocationSize = 1)
public class User extends GenericModel {

	@Column(name = "role_id", nullable = false, updatable = false, insertable = false)
	private Long roleId;

	@Column(name = "phone")
	private String phone;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "login", nullable = false)
	private String login;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "date_birth")
	private LocalDate birthDate;

	@Column(name = "backup_email", nullable = false)
	private String backUpEmail;

	@Column(name = "address")
	private String address;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<Publishing> publishingSet;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_ROLES_ID"))
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Role role;

}
