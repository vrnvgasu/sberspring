package ru.edu.sber_spring.model;

public enum GenreEnum {
	FANTASY("Фантастика"),
	SCIENCE_FICTION("Научная фантастика"),
	DRAMA("Драма"),
	NOVEL("Роман");

	private final String genreText;

	GenreEnum(String genreText) {
		this.genreText = genreText;
	}

	public String getGenreName() {
		return this.genreText;
	}
}
