package ru.edu.sber_spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "publishings")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "publishings_seq", allocationSize = 1)
public class Publishing extends GenericModel {

	@Column(name = "user_id", updatable = false, insertable = false)
	private Long userId;

	@Column(name = "book_id", updatable = false, insertable = false)
	private Long bookId;

	@Column(name = "returned", nullable = false)
	private Boolean returned;

	@Column(name = "return_date", nullable = false)
	private LocalDateTime returnDate;

	@Column(name = "return_period", nullable = false)
	private Integer returnPeriod;

	@Column(name = "rent_date", nullable = false)
	private LocalDateTime rentDate;

	@Column(name = "amount")
	private Integer amount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "book_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_BOOKS_ID"))
	@JsonIgnore
	private Book book;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_USERS_ID"))
	@JsonIgnore
	private User user;

}
