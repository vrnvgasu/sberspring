package ru.edu.sber_spring.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.edu.sber_spring.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByLogin(String login);

	User findUserByLogin(String login);

	User findByBackUpEmail(String email);

	@Query(nativeQuery = true,
			value = """
					select backup_email
					from users u join publishing p on u.id = p.user_id
					where p.return_date >= now()
					and p.returned = false
					                 """)
	List<String> getDelayedEmails();

}
