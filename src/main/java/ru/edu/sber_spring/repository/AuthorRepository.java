package ru.edu.sber_spring.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

	List<Author> findAuthorByBooksIn(Set<Book> books);

	List<Author> findAuthorByBooks(Book book);

	List<Author> findAuthorByBooksId(Long bookId);

	Author findByAuthorFIO(String fio);

	@Query(value = "select a from Author a "
			+ "left join fetch a.books b "
			+ "left join fetch b.authors "
			+ "where a.id=:authorId")
	Optional<Author> findByIdWithBooksAndAllAuthors(@Param(value = "authorId") Long authorId);

	@Query(value = "select a from Author a "
			+ "left join fetch a.books b "
			+ "where a.id=:authorId")
	Optional<Author> findByIdWithBooks(@Param(value = "authorId") Long authorId);

	List<Author> findAuthorByAuthorFIO(String authorFIO);

}
