package ru.edu.sber_spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.edu.sber_spring.model.Role;
import ru.edu.sber_spring.model.RoleEnum;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByTitle(RoleEnum title);

}
