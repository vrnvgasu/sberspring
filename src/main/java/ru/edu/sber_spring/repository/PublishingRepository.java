package ru.edu.sber_spring.repository;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.sber_spring.model.Publishing;

@Repository
public interface PublishingRepository extends JpaRepository<Publishing, Long> {

	Publishing findByUserId(Long id);

	@Query(value = "select p from Publishing p "
			+ "left join fetch p.book "
			+ "where p.userId=:userId and p.returned=false "
			+ "and p.returnDate < 'now()'")
	Set<Publishing> findUserLatePublishing(@Param("userId") Long userId);

	@Query(value = "select p.id from Publishing p "
			+ "left join p.book b "
			+ "left join b.authors a "
			+ "where a.id=:authorId and p.returned=false")
	Set<Long> findAllInRentByAuthor(@Param(value = "authorId") Long authorId);

	List<Publishing> findAllByBookTitleContainingIgnoreCase(String title);

}
