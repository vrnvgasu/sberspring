package ru.edu.sber_spring.repository;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {

	Book findBookByTitleLike(String title);

	Book findByTitle(String book1);

	List<Book> findBooksByAuthors(Author author);

	List<Book> findAllByIdNotIn(Set<Long> bookIdSet);

	@Query(nativeQuery = true,
			value = """
					  select *
					  from books b
					  left join books_authors ba on b.id = ba.book_id
					  left join authors a on a.id = ba.author_id
					  where lower(b.title) like '%' || lower(:title) || '%'
					  and cast(b.genre as char) like COALESCE(:genre, '%')
					  and coalesce(lower(a.fio), '%') like '%' || lower(:fio) || '%'
					""")
	List<Book> searchBooks(@Param(value = "genre") String genre,
			@Param(value = "title") String title,
			@Param(value = "fio") String fio);

	@Query(nativeQuery = true,
			value = """
					  select count(*)
					  from books b
					  join publishings p on b.id = p.book_id
					  and p.returned = false
					  and b.id = :bookId
					""")
	Integer getActivePublishingsCount(@Param(value = "bookId") Long bookId);

}
