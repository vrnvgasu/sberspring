package ru.edu.sber_spring.exception;

public class MyDeleteException
		extends Exception {

	public MyDeleteException(String message) {
		super(message);
	}

}
