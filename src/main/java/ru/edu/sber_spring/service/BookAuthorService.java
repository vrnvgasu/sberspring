package ru.edu.sber_spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.edu.sber_spring.dto.AuthorBookDTO;
import ru.edu.sber_spring.dto.AuthorDTO;
import ru.edu.sber_spring.dto.BookAuthorDTO;
import ru.edu.sber_spring.dto.BookDTO;
import ru.edu.sber_spring.dto.BookSearchDTO;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;

@Service
public class BookAuthorService {

	private final BookService bookService;

	private final AuthorService authorService;

	public BookAuthorService(BookService bookService, AuthorService authorService) {
		this.bookService = bookService;
		this.authorService = authorService;
	}

	public List<BookAuthorDTO> getAllBooksWithAuthors() {
		List<Book> books = bookService.listAll();
		List<AuthorDTO> authorDTOS;
		List<BookAuthorDTO> bookAuthorDTOList = new ArrayList<>();

		for (Book book : books) {
			authorDTOS = new ArrayList<>();
			for (Author author : book.getAuthors()) {
				AuthorDTO authorDTO = new AuthorDTO(author);
				authorDTOS.add(authorDTO);
			}
			BookAuthorDTO bookAuthorDTO = new BookAuthorDTO(book, authorDTOS);
			bookAuthorDTOList.add(bookAuthorDTO);
		}
		return bookAuthorDTOList;
	}

	public AuthorBookDTO getAuthorWithBooks(Long authorId) {
		List<BookDTO> bookDTOS = new ArrayList<>();
		Author author = authorService.getOne(authorId);

		for (Book book : author.getBooks()) {
			bookDTOS.add(new BookDTO(book));
		}

		return new AuthorBookDTO(author, bookDTOS);
	}

	public List<BookDTO> getAllBooksWhereAuthorNotIn(Long authorId) {
		AuthorBookDTO authorBookDTO = authorService.getAuthorWithBooks(authorId);
		Set<Long> bookIdSet = new HashSet<>();

		for (BookDTO bookDTO : authorBookDTO.getBookDTOS()) {
			bookIdSet.add(bookDTO.getId());
		}

		List<Book> books;

		if (bookIdSet.isEmpty()) {
			books = bookService.listAll();
		} else {
			books = bookService.listAllWhereIdNotIn(bookIdSet);
		}

		List<BookDTO> bookDTOS = new ArrayList<>();

		for (Book book : books) {
			bookDTOS.add(new BookDTO(book));
		}
		return bookDTOS;
	}

	public void addBookToAuthor(Long bookId, Long authorId) {
		Author author = authorService.getOne(authorId);
		Book book = bookService.getOne(bookId);

		book.getAuthors().add(author);
		author.getBooks().add(book);
		authorService.update(author);
	}

	public Page<BookAuthorDTO> getAllPaginated(Pageable pageRequest) {
		Page<Book> books = bookService.listAllPaginated(pageRequest);
		List<BookAuthorDTO> bookAuthorDTOList = getBookAuthorDTOs(books.getContent());
		return new PageImpl<>(bookAuthorDTOList, pageRequest, books.getTotalElements());
	}

	public Page<BookAuthorDTO> searchBooks(BookSearchDTO bookSearchDTO,
			Pageable pageable) {
		List<Book> books = bookService.findBooks(bookSearchDTO);
		List<BookAuthorDTO> bookAuthorDTOList = getBookAuthorDTOs(books);
		return new PageImpl<>(bookAuthorDTOList, pageable, books.size());
	}


	private List<BookAuthorDTO> getBookAuthorDTOs(List<Book> books) {
		List<AuthorDTO> authorDTOS;
		List<BookAuthorDTO> bookAuthorDTOList = new ArrayList<>();

		for (Book book : books) {
			authorDTOS = new ArrayList<>();
			for (Author author : book.getAuthors()) {
				AuthorDTO authorDTO = new AuthorDTO(author);
				authorDTOS.add(authorDTO);
			}
			BookAuthorDTO bookAuthorDTO = new BookAuthorDTO(book, authorDTOS);
			bookAuthorDTOList.add(bookAuthorDTO);
		}
		return bookAuthorDTOList;
	}

}
