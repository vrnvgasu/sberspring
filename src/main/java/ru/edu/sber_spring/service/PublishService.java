package ru.edu.sber_spring.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.sber_spring.dto.PublishDTO;
import ru.edu.sber_spring.dto.UserPublishDTO;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.model.Publishing;
import ru.edu.sber_spring.model.User;
import ru.edu.sber_spring.repository.BookRepository;
import ru.edu.sber_spring.repository.PublishingRepository;
import ru.edu.sber_spring.repository.UserRepository;

@Service
public class PublishService
		extends GenericService<Publishing, PublishDTO> {

	private final PublishingRepository publishingRepository;

	private final UserRepository userRepository;

	private final UserService userService;

	private final BookService bookService;

	private final BookRepository bookRepository;

	public PublishService(PublishingRepository publishingRepository, UserRepository userRepository,
			UserService userService, BookService bookService, BookRepository bookRepository) {
		this.publishingRepository = publishingRepository;
		this.userRepository = userRepository;
		this.userService = userService;
		this.bookService = bookService;
		this.bookRepository = bookRepository;
	}

	@Override
	public Publishing update(Publishing object) {
		return publishingRepository.save(object);
	}

	@Override
	public Publishing updateFromDTO(PublishDTO object, Long objectId) {
		Publishing publishing = publishingRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Publishing with such ID: " + objectId + " not found"));
		publishing.setRentDate(object.getReturnDateOutput());
		publishing.setReturnDate(object.getReturnDate());
		publishing.setReturnPeriod(object.getRentPeriod());
		publishing.setReturned(object.getReturned());

		return publishingRepository.save(publishing);
	}

	@Override
	public Publishing createFromDTO(PublishDTO newDtoObject) {
		Publishing newPublishing = new Publishing();
		newPublishing.setRentDate(newDtoObject.getReturnDateOutput());
		newPublishing.setReturnDate(newDtoObject.getReturnDate());
		newPublishing.setReturnPeriod(newDtoObject.getRentPeriod());
		newPublishing.setReturned(newDtoObject.getReturned());
		newPublishing.setCreatedBy(newDtoObject.getCreatedBy());
		newPublishing.setCreatedWhen(newDtoObject.getCreatedWhen());

		User user = userRepository.findById(newDtoObject.getUserId())
				.orElseThrow(() -> new NotFoundException("Such user with id=" + newDtoObject.getUserId() + " was not found"));
		Book book = bookRepository.findById(newDtoObject.getBookId())
				.orElseThrow(() -> new NotFoundException("Such book with id=" + newDtoObject.getBookId() + " was not found"));
		newPublishing.setUser(user);
		newPublishing.setBook(book);

		return publishingRepository.save(newPublishing);
	}

	@Override
	public Publishing createFromEntity(Publishing newObject) {
		return publishingRepository.save(newObject);
	}

	@Override
	public void delete(Long objectId) {
		Publishing role = publishingRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Publishing with such ID: " + objectId + " not found"));
		publishingRepository.delete(role);
	}

	@Override
	public Publishing getOne(Long objectId) {
		return publishingRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Publishing with such ID: " + objectId + " not found"));
	}

	@Override
	public List<Publishing> listAll() {
		return publishingRepository.findAll();
	}

	public Publishing close(Long publishingId) {
		Publishing publishing = publishingRepository.findById(publishingId)
				.orElseThrow(() -> new NotFoundException("Such publishing with id=" + publishingId + " was not found"));
		publishing.setReturned(true);
		return publishingRepository.save(publishing);
	}

	public Page<PublishDTO> getUserPublishing(Long id,
			Pageable pageable) {
		UserPublishDTO userPublishDTO = new UserPublishDTO(userService.getOne(id));
		return new PageImpl<>(
				new ArrayList<>(userPublishDTO.getPublishDTOSet()),
				pageable,
				userPublishDTO.getPublishDTOSet().size()
		);
	}

	public Page<PublishDTO> searchPublish(PublishDTO publishDTO,
			Pageable pageable) {
		List<Publishing> publishes = publishingRepository.findAllByBookTitleContainingIgnoreCase(publishDTO.getBookTitleSearch());
		List<PublishDTO> publishDTOS = new ArrayList<>();
		for (Publishing p : publishes) {
			publishDTOS.add(new PublishDTO(p));
		}
		return new PageImpl<>(publishDTOS, pageable, publishDTOS.size());
	}

	public void rentABook(Long bookId,
			String name,
			int count,
			int period) {
		User user = userService.getByUserName(name);
		Book book = bookService.getOne(bookId);
		book.setAmount(book.getAmount() - count);
		bookService.update(book);

		PublishDTO publishDTO = new PublishDTO(user, book, count, period);
		Publishing publish = new Publishing();
		publish.setBook(publishDTO.getBook());
		publish.setUser(publishDTO.getUser());
		publish.setRentDate(publishDTO.getReturnDateOutput());
		publish.setReturnPeriod(publishDTO.getRentPeriod());
		publish.setReturned(publishDTO.getReturned());
		publish.setReturnDate(publishDTO.getReturnDate());
		publish.setAmount(publishDTO.getAmount());
		publishingRepository.save(publish);
	}

	public void returnBook(Long publishId) {
		Publishing publish = getOne(publishId);
		publish.setReturned(true);
		Book book = publish.getBook();
		book.setAmount(book.getAmount() + publish.getAmount());
		publishingRepository.save(publish);
		bookService.update(book);
	}

}
