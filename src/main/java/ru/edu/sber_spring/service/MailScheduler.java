package ru.edu.sber_spring.service;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MailScheduler {

	private UserService userService;

	private JavaMailSender emailSender;

	@Autowired
	public void getUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void getEmailService(JavaMailSender emailSender) {
		this.emailSender = emailSender;
	}

	//https://crontab.guru/
	//http://www.cronmaker.com/?1
	@Scheduled(cron = "0 0 12 * * ?")
	public void sendMailsToDebtors() {
		SimpleMailMessage message = new SimpleMailMessage();
		List<String> emails = userService.getUserEmailsWithDelayedRentDate();
		if (emails.size() > 0) {
			message.setTo(emails.toArray(new String[0]));
			message.setSubject("Напоминание о просрочке книги.");
			message.setText(
					"Добрый день. Вы получили это письмо, так как одна из ваших книг просрочена. Пожалуйста, верните ее в библиотеку.");
			emailSender.send(message);
		}
		log.info("Планировщик работает!");
	}

}
