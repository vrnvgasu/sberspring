package ru.edu.sber_spring.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import ru.edu.sber_spring.dto.BookAuthorDTO;
import ru.edu.sber_spring.dto.BookDTO;
import ru.edu.sber_spring.dto.BookSearchDTO;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.model.GenreEnum;
import ru.edu.sber_spring.repository.AuthorRepository;
import ru.edu.sber_spring.repository.BookRepository;

@Service
@Slf4j
public class BookService
		extends GenericService<Book, BookAuthorDTO> {

	private static final String UPLOAD_DIRECTORY = "files/books";

	private final BookRepository bookRepository;

	private final AuthorRepository authorRepository;

	public BookService(BookRepository bookRepository,
			AuthorRepository authorRepository) {
		this.authorRepository = authorRepository;
		this.bookRepository = bookRepository;
	}

	@Override
	public Book update(Book object) {
		return bookRepository.save(object);
	}

	@Override
	public Book updateFromDTO(BookAuthorDTO object, Long bookId) {
		Book book = bookRepository.findById(bookId).orElseThrow(
				() -> new NotFoundException("Such book with id=" + bookId + " now found"));
		book.setTitle(object.getTitle());
		book.setAmount(object.getAmount());
		book.setGenre(object.getGenre());
		book.setPublishYear(object.getPublishYear());
		book.setStoragePlace(object.getStoragePlace());
		book.setOnlineCopy(object.getOnlineCopy());
		Set<Author> authors = new HashSet<>(authorRepository.findAllById(object.getAuthorIds()));

		//TODO: подумать что делать если хотим убрать автора из книги
		//TODO: создать автора "Без Автора" и выставлять его при удалении автора (если он единственный у книги).
		book.setAuthors(authors);
		return bookRepository.save(book);
	}

	@Override
	public Book createFromDTO(BookAuthorDTO newDtoObject) {
		Book book = new Book();
		book.setTitle(newDtoObject.getTitle());
		book.setAmount(newDtoObject.getAmount());
		book.setGenre(newDtoObject.getGenre());
		book.setOnlineCopy(newDtoObject.getOnlineCopy());
		book.setPublishYear(newDtoObject.getPublishYear());
		book.setStoragePlace(newDtoObject.getStoragePlace());
		book.setCreatedBy(newDtoObject.getCreatedBy());
		book.setCreatedWhen(newDtoObject.getCreatedWhen());

		Set<Author> authors = new HashSet<>(authorRepository.findAllById(newDtoObject.getAuthorIds()));

		book.setAuthors(authors);
		return bookRepository.save(book);
	}

	public Book createFromDTO(BookDTO bookDTO) {
		Book book = new Book();
		book.setTitle(bookDTO.getTitle());
		book.setAmount(bookDTO.getAmount());
		book.setGenre(bookDTO.getGenre());
		book.setOnlineCopy(bookDTO.getOnlineCopy());
		book.setPublishYear(bookDTO.getPublishYear());
		book.setStoragePlace(bookDTO.getStoragePlace());
		book.setCreatedBy(bookDTO.getCreatedBy());
		book.setCreatedWhen(bookDTO.getCreatedWhen());
		return bookRepository.save(book);
	}

	@Override
	public Book createFromEntity(Book newObject) {
		return bookRepository.save(newObject);
	}

	@Override
	public void delete(Long objectId) {
		Book book = bookRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Such book with id=" + objectId + " now found"));
		//TODO:ДЗ № 11: дописать проверку на возможность удалить книгу - сделать проверку на то, что returned != true
		if (bookRepository.getActivePublishingsCount(objectId) == 0) {
			bookRepository.delete(book);
		} else {
			throw new RuntimeException("Книга не может быть удалена, так как у нее есть активные аренды.");
		}
	}

	@Override
	public Book getOne(Long objectId) {
		return bookRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Such book with id=" + objectId + " now found"));
	}

	@Override
	public List<Book> listAll() {
		return bookRepository.findAll();
	}

	public List<Book> listAllWhereIdNotIn(Set<Long> bookIdSet) {
		return bookRepository.findAllByIdNotIn(bookIdSet);
	}

	public List<Book> search(String title, String authorFio, GenreEnum genre) {
		Specification<Book> specification = (root, cq, cb) -> null;

		if (!Objects.isNull(title)) {
			specification = specification.and((root, cq, cb) ->
					cb.like(cb.lower(root.get("title")), "%" + title.toLowerCase() + "%"));
		}
		if (!Objects.isNull(genre)) {
			specification = specification.and((root, cq, cb) -> cb.equal(root.get("genre"), genre));
		}
		if (!Objects.isNull(authorFio)) {
			specification = specification.and((root, cq, cb) -> {
				cq.distinct(true);
				Root<Book> bookRoot = root;
				Root<Author> authorRoot = cq.from(Author.class);
				Expression<Collection<Book>> authorBooks = authorRoot.get("books");
				return cb.and(
						cb.like(cb.lower(authorRoot.get("authorFIO")), "%" + authorFio.toLowerCase() + "%"),
						cb.isMember(bookRoot, authorBooks)
				);
			});
		}

		return bookRepository.findAll(specification);
	}

	public Page<Book> listAllPaginated(Pageable pageRequest) {
		Page<Book> books = bookRepository.findAll(pageRequest);
		return new PageImpl<>(books.getContent(), pageRequest, books.getTotalElements());
	}

	public List<Book> findBooks(BookSearchDTO bookSearchDTO) {
		String genre = bookSearchDTO.getGenre() != null ? String.valueOf(bookSearchDTO.getGenre().ordinal()) : "%";
		return bookRepository.searchBooks(
				genre,
				bookSearchDTO.getBookTitle(),
				bookSearchDTO.getAuthorFio()
		);
	}

	public Book createFromDTO(BookDTO newDtoObject, MultipartFile file) {
		String fileName = createFile(file);
		newDtoObject.setOnlineCopy(fileName);
		return createFromDTO(newDtoObject);
	}

	private String createFile(MultipartFile file) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		String resultFileName = "";
		try {
			Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
			if (!path.toFile().exists()) {
				Files.createDirectories(path);
			}
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
			resultFileName = UPLOAD_DIRECTORY + "/" + fileName;
		} catch (IOException e) {
			log.error("BookService#createFile(): {}", e.getMessage());
		}
		return resultFileName;
	}

	public Book updateFromDTO(BookDTO object, Long bookId, MultipartFile file) {
		String fileName = createFile(file);
		object.setOnlineCopy(fileName);
		return updateFromDTO(object, bookId);
	}

	public Book updateFromDTO(BookDTO object, Long bookId) {
		Book book = bookRepository.findById(bookId).orElseThrow(
				() -> new NotFoundException("Such book with id=" + bookId + " now found"));
		book.setTitle(object.getTitle());
		book.setAmount(object.getAmount());
		book.setGenre(object.getGenre());
		book.setPublishYear(object.getPublishYear());
		book.setStoragePlace(object.getStoragePlace());
		book.setOnlineCopy(object.getOnlineCopy());
		if (object instanceof BookAuthorDTO) {
			Set<Author> authors = new HashSet<>(authorRepository.findAllById(((BookAuthorDTO) object).getAuthorIds()));
			book.setAuthors(authors);
		}
		return bookRepository.save(book);
	}

}
