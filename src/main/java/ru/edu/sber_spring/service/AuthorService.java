package ru.edu.sber_spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.sber_spring.dto.AuthorBookDTO;
import ru.edu.sber_spring.dto.AuthorDTO;
import ru.edu.sber_spring.dto.BookDTO;
import ru.edu.sber_spring.model.Author;
import ru.edu.sber_spring.model.Book;
import ru.edu.sber_spring.repository.AuthorRepository;
import ru.edu.sber_spring.repository.BookRepository;
import ru.edu.sber_spring.repository.PublishingRepository;

@Service
public class AuthorService
		extends GenericService<Author, AuthorDTO> {

	private final AuthorRepository authorRepository;

	private final BookRepository bookRepository;

	private final PublishingRepository publishingRepository;


	public AuthorService(AuthorRepository authorRepository,
			BookRepository bookRepository, PublishingRepository publishingRepository) {
		this.authorRepository = authorRepository;
		this.bookRepository = bookRepository;
		this.publishingRepository = publishingRepository;
	}

	@Override
	public Author update(Author object) {
		return authorRepository.save(object);
	}

	@Override
	public Author updateFromDTO(AuthorDTO object, Long authorId) {
		Author author = authorRepository.findById(authorId).orElseThrow(
				() -> new NotFoundException("Author with such ID: " + authorId + " not found"));
		author.setAuthorFIO(object.getAuthorFIO());
		author.setLifePeriod(object.getLifePeriod());
		author.setDescription(object.getDescription());
		return authorRepository.save(author);
	}

	@Override
	public Author createFromDTO(AuthorDTO newDtoObject) {
		Author newAuthor = new Author();
		newAuthor.setAuthorFIO(newDtoObject.getAuthorFIO());
		newAuthor.setDescription(newDtoObject.getDescription());
		newAuthor.setLifePeriod(newDtoObject.getLifePeriod());
		newAuthor.setCreatedBy(newDtoObject.getCreatedBy());
		newAuthor.setCreatedWhen(newDtoObject.getCreatedWhen());
		return authorRepository.save(newAuthor);
	}

	@Override
	public Author createFromEntity(Author newObject) {
		return authorRepository.save(newObject);
	}

	@Override
	public void delete(Long objectId) {
		if (!publishingRepository.findAllInRentByAuthor(objectId).isEmpty()) {
			throw new RuntimeException("There are books for rent with the author ID:" + objectId);
		}

		Author author = authorRepository.findByIdWithBooksAndAllAuthors(objectId).orElseThrow(
				() -> new NotFoundException("Author with such ID: " + objectId + " not found"));

		for (Book book : author.getBooks()) {
			book.getAuthors().remove(author);

			if (book.getAuthors().size() > 1) {
				continue;
			}

			bookRepository.deleteById(book.getId());
		}

		author.setBooks(new HashSet<>());
		authorRepository.deleteById(author.getId());
	}

	@Override
	public Author getOne(Long objectId) {
		return authorRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Author with such ID: " + objectId + " not found"));
	}

	@Override
	public List<Author> listAll() {
		return authorRepository.findAll();
	}

	/**
	 * Ищем все книги заданного автора.
	 *
	 * @param authorId Айди автора
	 * @return List<Book> список книг автора
	 */
	public List<BookDTO> getAllAuthorBooks(Long authorId) {
		Author author = authorRepository.findById(authorId).orElseThrow(
				() -> new NotFoundException("Author with such ID: " + authorId + " not found"));
		List<BookDTO> bookDTOList = new ArrayList<>();
		for (Book book : bookRepository.findBooksByAuthors(author)) {
			BookDTO bookDTO = new BookDTO(book);
			bookDTOList.add(bookDTO);
		}
		return bookDTOList;
	}

	public AuthorBookDTO getAuthorWithBooks(Long authorId) {
		Author author = authorRepository.findByIdWithBooks(authorId).orElseThrow(
				() -> new NotFoundException("Author with such ID: " + authorId + " not found"));
		List<BookDTO> bookDTOList = new ArrayList<>();

		for (Book book : bookRepository.findBooksByAuthors(author)) {
			BookDTO bookDTO = new BookDTO(book);
			bookDTOList.add(bookDTO);
		}

		return new AuthorBookDTO(author, bookDTOList);
	}

	public List<Author> findAuthorByAuthorFIO(String authorFIO) {
		return authorRepository.findAuthorByAuthorFIO(authorFIO);
	}

}
