package ru.edu.sber_spring.service;

import java.util.List;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.sber_spring.dto.RoleDTO;
import ru.edu.sber_spring.model.Role;
import ru.edu.sber_spring.repository.RoleRepository;

@Service
public class RoleService
		extends GenericService<Role, RoleDTO> {

	private final RoleRepository roleRepository;

	public RoleService(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public Role update(Role object) {
		return roleRepository.save(object);
	}

	@Override
	public Role updateFromDTO(RoleDTO object, Long roleId) {
		Role role = roleRepository.findById(roleId).orElseThrow(
				() -> new NotFoundException("Role with such ID: " + roleId + " not found"));
		role.setTitle(object.getTitle());
		role.setDescription(object.getDescription());
		return roleRepository.save(role);
	}

	@Override
	public Role createFromDTO(RoleDTO newDtoObject) {
		Role newRole = new Role();
		newRole.setTitle(newDtoObject.getTitle());
		newRole.setDescription(newDtoObject.getDescription());
		newRole.setCreatedBy(newDtoObject.getCreatedBy());
		newRole.setCreatedWhen(newDtoObject.getCreatedWhen());
		return roleRepository.save(newRole);
	}

	@Override
	public Role createFromEntity(Role newObject) {
		return roleRepository.save(newObject);
	}

	@Override
	public void delete(Long objectId) {
		Role role = roleRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Role with such ID: " + objectId + " not found"));
		roleRepository.delete(role);
	}

	@Override
	public Role getOne(Long objectId) {
		return roleRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("Role with such ID: " + objectId + " not found"));
	}

	@Override
	public List<Role> listAll() {
		return roleRepository.findAll();
	}

}
