package ru.edu.sber_spring.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.sber_spring.dto.BookDTO;
import ru.edu.sber_spring.dto.LatePublishingBookDTO;
import ru.edu.sber_spring.dto.LoginDTO;
import ru.edu.sber_spring.dto.UserDTO;
import ru.edu.sber_spring.model.Publishing;
import ru.edu.sber_spring.model.Role;
import ru.edu.sber_spring.model.User;
import ru.edu.sber_spring.repository.PublishingRepository;
import ru.edu.sber_spring.repository.RoleRepository;
import ru.edu.sber_spring.repository.UserRepository;

@Service
public class UserService
		extends GenericService<User, UserDTO> {

	private static final String CHANGE_PASSWORD_URL = "http://localhost:9090/users/change-password/";

	private final UserRepository userRepository;

	private final RoleRepository roleRepository;

	private final PublishingRepository publishingRepository;

	private final JavaMailSender javaMailSender;

	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserService(UserRepository userRepository, RoleRepository roleRepository,
			PublishingRepository publishingRepository, JavaMailSender javaMailSender,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.publishingRepository = publishingRepository;
		this.javaMailSender = javaMailSender;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public User update(User object) {
		Role role = roleRepository.findById(object.getRoleId())
				.orElseThrow(() -> new NotFoundException("Such role with id=" + object.getRoleId() + " was not found"));
		object.setRole(role);

		return userRepository.save(object);
	}

	@Override
	public User updateFromDTO(UserDTO object, Long objectId) {
		User user = userRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("User with such ID: " + objectId + " not found"));
		user.setAddress(object.getAddress());
		user.setBackUpEmail(object.getBackUpEmail());
		user.setBirthDate(LocalDate.parse(object.getBirthDate()));
		user.setFirstName(object.getFirstName());
		user.setLastName(object.getLastName());
		user.setLogin(object.getLogin());
		user.setMiddleName(object.getMiddleName());
		user.setPhone(object.getPhone());

		if (object.getRoleId() != null) {
			Role role = roleRepository.findById(object.getRoleId())
					.orElseThrow(() -> new NotFoundException("Such role with id=" + object.getRoleId() + " was not found"));
			user.setRole(role);
		}

		return userRepository.save(user);
	}

	@Override
	public User createFromDTO(UserDTO newDtoObject) {
		User newUser = new User();
		newUser.setAddress(newDtoObject.getAddress());
		newUser.setBackUpEmail(newDtoObject.getBackUpEmail());
		newUser.setBirthDate(LocalDate.parse(newDtoObject.getBirthDate()));
		newUser.setFirstName(newDtoObject.getFirstName());
		newUser.setLastName(newDtoObject.getLastName());
		newUser.setLogin(newDtoObject.getLogin());
		newUser.setPassword(bCryptPasswordEncoder.encode(newDtoObject.getPassword()));
		newUser.setMiddleName(newDtoObject.getMiddleName());
		newUser.setPhone(newDtoObject.getPhone());
		newUser.setCreatedBy(newDtoObject.getCreatedBy());
		newUser.setCreatedWhen(newDtoObject.getCreatedWhen());

		Role role = roleRepository.findById(newDtoObject.getRoleId() == null ? 1L : newDtoObject.getRoleId())
				.orElseThrow(() -> new NotFoundException("Such role with id=" + newDtoObject.getRoleId() + " was not found"));
		newUser.setRole(role);

		return userRepository.save(newUser);
	}

	@Override
	public User createFromEntity(User newObject) {
		Role role = roleRepository.findById(newObject.getRoleId())
				.orElseThrow(() -> new NotFoundException("Such role with id=" + newObject.getRoleId() + " was not found"));
		newObject.setRole(role);
		return userRepository.save(newObject);
	}

	@Override
	public void delete(Long objectId) {
		User role = userRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("User with such ID: " + objectId + " not found"));
		userRepository.delete(role);
	}

	@Override
	public User getOne(Long objectId) {
		return userRepository.findById(objectId).orElseThrow(
				() -> new NotFoundException("User with such ID: " + objectId + " not found"));
	}

	@Override
	public List<User> listAll() {
		return userRepository.findAll();
	}

	public Set<LatePublishingBookDTO> getAllUsersLateReturnedBooks(Long userId) {
		Set<LatePublishingBookDTO> latePublishingBookDTOS = new HashSet<>();

		for (Publishing publishing : publishingRepository.findUserLatePublishing(userId)) {
			LatePublishingBookDTO latePublishingBookDTO = new LatePublishingBookDTO(publishing);

			Period period = Period.between(publishing.getReturnDate().toLocalDate(), LocalDateTime.now().toLocalDate());
			latePublishingBookDTO.setLatePeriod(period.getDays());

			BookDTO bookDTO = new BookDTO(publishing.getBook());
			latePublishingBookDTO.setBookDTO(bookDTO);

			latePublishingBookDTOS.add(latePublishingBookDTO);
		}

		return latePublishingBookDTOS;
	}

	public UserDTO getUserByEmail(final String email) {
		return new UserDTO(userRepository.findByBackUpEmail(email));
	}

	public void sendChangePasswordEmail(final String backUpEmail,
			final Long userId) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(backUpEmail);
		message.setSubject("Восстановление пароля на сервисе \"Онлайн Библиотека\"");
		message.setText(
				"Добрый день! Вы получили это письмо, так как с вашего аккаунта была отправлена заявка на восстановление пароля. "
						+
						"Для восстановления пароля, перейдите по ссылке: '" + CHANGE_PASSWORD_URL + userId + "'");
		javaMailSender.send(message);

	}

	public void changePassword(Long userId, String password) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new NotFoundException("User with usch ID: " + userId + " not found."));
		user.setPassword(bCryptPasswordEncoder.encode(password));
		userRepository.save(user);
	}

	public User getByUserName(final String name) {
		return userRepository.findUserByLogin(name);
	}

	public List<String> getUserEmailsWithDelayedRentDate() {
		return userRepository.getDelayedEmails();
	}

	public boolean checkPassword(LoginDTO loginDTO) {
		return bCryptPasswordEncoder.matches(
				loginDTO.getPassword(),
				getByUserName(loginDTO.getUsername()).getPassword()
		);
	}

	public User findById(Long userId) {
		return userRepository.findById(userId).orElseThrow(
				() -> new NotFoundException("User with such ID: " + userId + " not found"));
	}

}
